from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

import tasks.views


router = DefaultRouter()
router.register(r'projects', tasks.views.ProjectViewSet)
router.register(r'sprints', tasks.views.SprintViewSet)
router.register(r'tasks', tasks.views.TaskViewSet)

urlpatterns = [
    url(r'', include(router.urls))
]
