# Generated by Django 2.0.5 on 2018-05-30 09:17

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0003_auto_20180530_0916'),
    ]

    operations = [
        migrations.RenameField(
            model_name='sprint',
            old_name='descirption',
            new_name='description',
        ),
    ]
