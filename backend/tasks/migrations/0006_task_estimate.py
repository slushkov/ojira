# Generated by Django 2.0.5 on 2018-05-30 09:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0005_auto_20180530_0919'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='estimate',
            field=models.FloatField(null=True),
        ),
    ]
