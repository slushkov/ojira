from django.db.models import Model, CharField, TextField, ManyToManyField, ForeignKey, DateField, CASCADE, BooleanField, FloatField


class Label(Model):
    name = CharField(max_length=32)


class Project(Model):
    name = CharField(max_length=32)
    code = CharField(max_length=8)


class Sprint(Model):
    name = CharField(max_length=128)
    description = TextField(blank=True, null=True)
    finished = BooleanField(default=False)
    is_edit_allowed = BooleanField(default=True)
    is_finish_allowed = BooleanField(default=True)


class Task(Model):
    sprint = ForeignKey(Sprint, related_name='tasks_ids', on_delete=CASCADE)
    name = CharField(max_length=128)
    description = TextField(null=True)
    labels = ManyToManyField(Label, blank=True)
    estimate = FloatField(null=True)
    done = BooleanField(default=False)
