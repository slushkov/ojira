from rest_framework.serializers import ModelSerializer

from . import models 


class ProjectSerializer(ModelSerializer):
    class Meta:
        fields = '__all__'
        model = models.Project


class LabelSerializer(ModelSerializer):
    class Meta:
        fields = '__all__'
        model = models.Label


class TaskSerializer(ModelSerializer):
    class Meta:
        fields = '__all__'
        model = models.Task


class SprintSerializer(ModelSerializer):
    class Meta:
        fields = ('id', 'name', 'description', 'tasks_ids', 'finished', 'is_edit_allowed', 'is_finish_allowed')
        model = models.Sprint
