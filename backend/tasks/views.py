from rest_framework.viewsets import ModelViewSet

from . import models as models
from . import serializers as serializers


class ProjectViewSet(ModelViewSet):
    queryset = models.Project.objects.all()
    serializer_class = serializers.ProjectSerializer


class SprintViewSet(ModelViewSet):
    queryset = models.Sprint.objects.all()
    serializer_class = serializers.SprintSerializer


class TaskViewSet(ModelViewSet):
    queryset = models.Task.objects.all()
    serializer_class = serializers.TaskSerializer
