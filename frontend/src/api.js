import axiosRest from 'axios-rest-client'

const api = axiosRest({
  baseUrl: `http://${process.env.API_HOST}:${process.env.API_PORT}`,
  headers: {
    // 'Authorization': 'Basic YXBpOnBhc3N3b3Jk'
  }
})

api._axios.interceptors.request.use((config) => {
  config.url += '/'
  return config
})

export default api
