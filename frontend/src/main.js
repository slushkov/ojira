import Vue from 'vue'
import Router from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import VModal from 'vue-js-modal'

Vue.config.productionTip = false

Vue.use(BootstrapVue)
Vue.use(VModal, { dynamic: true })
Vue.use(Router)

/* eslint-disable import/first */
import App from './App'
import router from './router'
import store from './store'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  created() {
    this.$store.dispatch('projects.fetch')
    this.$store.dispatch('sprints.fetch')
    this.$store.dispatch('tasks.fetch')
  },
  components: { App },
  template: '<App/>'
})
