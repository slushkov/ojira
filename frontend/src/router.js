import Router from 'vue-router'

import Project from '@/pages/Project'
import ProjectsList from '@/pages/ProjectsList'

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Projects',
      component: ProjectsList,
      props: true
    },
    {
      path: '/:projectCode',
      name: 'Project',
      component: Project,
      props: true
    }
  ]
})
