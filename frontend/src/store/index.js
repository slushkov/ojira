import Vue from 'vue'
import Vuex from 'vuex'

import projects from './projects'
import sprints from './sprints'
import tasks from './tasks'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    projects: [],
    tasks: [],
    sprints: []
  },
  mutations: {
    ...projects.mutations,
    ...sprints.mutations,
    ...tasks.mutations
  },
  actions: {
    ...projects.actions,
    ...sprints.actions,
    ...tasks.actions
  },
  getters: {
    ...projects.getters,
    ...sprints.getters,
    ...tasks.getters
  }
})
