import api from '@/api'

const adapters = {
  frontend2api(p) {
    const {...sameFields} = p

    return {
      ...sameFields
    }
  },
  api2frontend(p) {
    const {...sameFields} = p

    return {
      ...sameFields
    }
  }
}

const mutations = {
  'projects.create'(state, project) {
    state.projects.push(project)
  }
}

const actions = {
  async 'projects.fetch'({commit}) {
    const projects = (await api.projects.all()).data.map(item => adapters.api2frontend(item))
    projects.forEach((proj) => commit('projects.create', proj))
  }
}

const getters = {
  projects: (state) => state.projects,
  project(state) {
    return (code) => {
      return state.projects.find((proj) => proj.code === code)
    }
  }
}

export default { mutations, actions, getters }
