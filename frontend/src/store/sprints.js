import api from '@/api'

const adapters = {
  api2frontend: (s) => {
    // eslint-disable-next-line
    const {is_edit_allowed, is_finish_allowed, tasks_ids, ...sameFields} = s

    return {
      ...sameFields,
      allowedActions: {
        edit: is_edit_allowed,
        finish: is_finish_allowed
      },
      tasksIds: tasks_ids
    }
  },
  frontend2api: (s) => {
    const {allowedActions, tasksIds, ...sameFields} = s

    // eslint-disable-next-line
    return {
      ...sameFields,
      is_edit_allowed: allowedActions.edit,
      is_finish_allowed: allowedActions.finish,
      tasks_ids: tasksIds
    }
  }
}

const mutations = {
  'sprints.deleteTask'(state, {sprintId, taskId}) {
    const idx = state.sprints.findIndex((s) => s.id === sprintId)

    let sprint = state.sprints[idx]
    sprint.tasksIds.splice(sprint.tasksIds.indexOf(taskId), 1)

    state.sprints[idx] = sprint
  },
  'sprints.addTask'(state, {sprintId, taskId}) {
    const idx = state.sprints.findIndex((s) => s.id === sprintId)

    let sprint = state.sprints[idx]
    sprint.tasksIds.push(taskId)

    state.sprints[idx] = sprint
  },
  'sprints.toggleFinish'(state, {id}) {
    const idx = state.sprints.findIndex((s) => s.id === id)
    state.sprints[idx].finished = !state.sprints[idx].finished
  },
  'sprints.setName'(state, { id, value }) {
    const idx = state.sprints.findIndex((s) => s.id === id)
    state.sprints[idx].name = value
  },
  'sprints.setDescription'(state, { id, value }) {
    const idx = state.sprints.findIndex((s) => s.id === id)
    state.sprints[idx].description = value
  },
  'sprints.setBeginDate'(state, { id, value }) {
    const idx = state.sprints.findIndex((s) => s.id === id)
    state.sprints[idx].beginDate = value
  },
  'sprints.setEndDate'(state, { id, value }) {
    const idx = state.sprints.findIndex((s) => s.id === id)
    state.sprints[idx].endDate = value
  },
  'sprints.create'(state, sprint) {
    state.sprints.push(sprint)
  }
}

const actions = {
  'sprints.update'({commit, getters}, sprint) {
    // eslint-disable-next-line
    const {id, name, description, beginDate, endDate, ..._} = sprint

    commit('sprints.setName', { id, value: name })
    commit('sprints.setDescription', { id, value: description })
    commit('sprints.setBeginDate', { id, value: beginDate })
    commit('sprints.setEndDate', { id, value: endDate })

    api.sprints.update(sprint.id, adapters.frontend2api(getters.sprint(sprint.id)))
  },
  'sprints.toggleFinish'({commit, getters}, {id}) {
    commit('sprints.toggleFinish', {id})
    api.sprints.update(id, adapters.frontend2api(getters.sprint(id)))
  },
  'sprints.create'({commit, getters}, fields) {
    const existedIds = getters.sprints.map((s) => s.id)
    const id = Math.max.apply(null, existedIds) + 1

    const sprint = {
      id,
      ...fields,
      allowedActions: {
        edit: true,
        finish: true
      },
      finished: false,
      tasksIds: []
    }

    commit('sprints.create', sprint)
    api.sprints.create(adapters.frontend2api(sprint))
  },
  async 'sprints.fetch'({commit}) {
    const sprints = (await api.sprints.all()).data.map(item => adapters.api2frontend(item))
    sprints.forEach((s) => commit('sprints.create', s))
  }
}

const getters = {
  sprints: state => state.sprints.sort((a, b) => a.id < b.id),
  activeSprints(state, getters) {
    return getters.sprints.filter((s) => s.finished === false)
  },
  sprint(state, getters) {
    return (id) => {
      return getters.sprints.find((sprint) => sprint.id === id)
    }
  },
  sprintTasks(state, getters) {
    return (sprintId) => {
      return getters.sprint(sprintId).tasksIds.map((id) => getters.task(id))
    }
  },
  sprintByTaskId(state, getters) {
    return (taskId) => {
      return getters.sprints.find((sprint) => {
        return sprint.tasksIds.indexOf(taskId) > -1
      })
    }
  }
}

export default { mutations, actions, getters }
