import api from '@/api'

const adapters = {
  api2frontend: (t) => {
    const {...sameFields} = t

    return {
      ...sameFields
    }
  },
  frontend2api: (t) => {
    const {...sameFields} = t

    return {
      ...sameFields
    }
  }
}

const mutations = {
  'tasks.delete'(state, {id}) {
    const idx = state.tasks.findIndex((t) => t.id === id)
    state.tasks.splice(idx, 1)
  },
  'tasks.setName'(state, {id, value}) {
    state.tasks.find((t) => t.id === id)
    const idx = state.tasks.findIndex((t) => t.id === id)
    state.tasks[idx].name = value
  },
  'tasks.setEstimate'(state, {id, value}) {
    const idx = state.tasks.findIndex((t) => t.id === id)
    state.tasks[idx].estimate = value
  },
  'tasks.toggleDone'(state, {id}) {
    const idx = state.tasks.findIndex((t) => t.id === id)
    state.tasks[idx].done = !state.tasks[idx].done
  },
  'tasks.create'(state, task) {
    state.tasks.push(task)
  }
}

const actions = {
  'task.toggleDone'({commit, getters}, {id}) {
    commit('tasks.toggleDone', {id})
    api.tasks.update(
      id,
      adapters.frontend2api({...getters.task(id), sprint: getters.sprintByTaskId(id).id})
    )
  },
  'task.delete'({commit, getters}, {id}) {
    commit('sprints.deleteTask', {
      sprintId: getters.sprintByTaskId(id).id,
      taskId: id
    })
    commit('tasks.delete', {id})
    api.tasks.delete(id)
  },
  'task.changeSprint'({commit, getters}, {taskId, moveToSprintId}) {
    commit('sprints.deleteTask', {
      sprintId: getters.sprintByTaskId(taskId).id,
      taskId
    })
    commit('sprints.addTask', {
      sprintId: moveToSprintId,
      taskId
    })
    api.tasks.update(
      taskId,
      adapters.frontend2api({...getters.task(taskId), sprint: getters.sprintByTaskId(taskId).id})
    )
  },
  'task.create'({commit, getters}, {name, sprintId}) {
    const id = Math.max.apply(null, getters.tasks.map((t) => t.id)) + 1

    const task = {
      id,
      name,
      labels: [],
      estimate: null
    }

    commit('tasks.create', task)
    commit('sprints.addTask', {
      sprintId,
      taskId: id
    })

    api.tasks.create(adapters.frontend2api({
      ...task,
      sprint: getters.sprintByTaskId(task.id).id
    }))
  },
  'task.update'({commit, getters}, task) {
    commit('tasks.setName', {id: task.id, value: task.name})
    commit('tasks.setEstimate', {id: task.id, value: task.estimate})
    api.tasks.update(
      task.id,
      adapters.frontend2api({...task, sprint: getters.sprintByTaskId(task.id).id})
    )
  },
  async 'tasks.fetch'({commit}) {
    const tasks = (await api.tasks.all()).data.map(item => adapters.api2frontend(item))
    tasks.forEach((t) => commit('tasks.create', t))
  }
}

const getters = {
  tasks: state => state.tasks,
  task(state, getters) {
    return (id) => {
      return getters.tasks.find((task) => task.id === id)
    }
  },
  taskPossibleMoveTo(state, getters) {
    return (id) => {
      const sprint = getters.sprintByTaskId(id)
      return getters.activeSprints.filter((s) => s.id !== sprint.id)
    }
  }
}

export default { mutations, actions, getters }
